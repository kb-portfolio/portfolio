$(function () {
    // navbar shrink
    const $topOffset = $('html, body').scrollTop();
    if ($topOffset > 50) {
        $('#page-navbar').addClass('navbar-shrink');
    }

    $(window).scroll(function () {
        if ($(document).scrollTop() > 50) {
            $('#page-navbar').addClass('navbar-shrink');
        } else {
            $('#page-navbar').removeClass('navbar-shrink');
        }
    });
    // end of navbar shrink

    // navbar scroll
    function scrollToSection(ev) {
        ev.preventDefault();
        const $section = $($(this).attr('href'));
        $('html, body').animate({
            scrollTop: $section.offset().top - 75
        }, 500);
    }
    $('[data-scroll]').on('click', scrollToSection);

    $('#header .banner-button').on('click', function () {
        $('html, body').animate({
            scrollTop: $('#about-me').offset().top - 75
        }, 500);
    });
    // end of navbar scroll

    //skill slider
    $('.skills-slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: true,
        dots: true,
        swipeToSlide: true,
        touchThreshold: 50,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    centerMode: true,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    centerMode: false,
                }
            }
        ]
    });
    //end of skill slider

    $('#projects #filter').on('click', 'li', function (ev) {
        ev.preventDefault();

        $('#projects #filter li.active').removeClass('active');

        $(ev.target).addClass('active');
        const currentCategory = $(this).find('.filter-link').data('filter')
        $.map($('#project-list li'), function (item) {
            console.log($(item));
            if ($(item).data('category') === currentCategory) {
                $(item).css('display', 'block');
            } else if (currentCategory === 'all') {
                $(item).css('display', 'block');
            } else {
                $(item).css('display', 'none');
            }
        });
    });

    $('#contact contactForm').submit(function () {
        sendContactForm();
        return false;
    });

});